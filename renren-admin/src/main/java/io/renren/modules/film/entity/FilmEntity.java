package io.renren.modules.film.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
@Data
@TableName("film")
public class FilmEntity {

    /**
     * 电影编号
     */
    @TableId(type = IdType.AUTO)
	private Integer filmId;
    /**
     * 电影名
     */
    private String filmName;
    /**
     * 电影时长
     */
	private String filmTime;
    /**
     * 上映时间
     */
	private Date playTime;
    /**
     * 电影图片
     */
	private String posterUrl;
    /**
     * 电影种类
     */
        private String filmCategory;
    /**
     * 导演
     */
	private String director;
    /**
     * 编剧
     */
	private String protagonist;
    /**
     * 电影种类
     */
	private String screenwriters;
    /**
     * 电影简介
     */
	private String filmReview;
    /**
     * 票价
     */
	private Double money;
}