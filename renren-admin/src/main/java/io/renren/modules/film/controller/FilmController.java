package io.renren.modules.film.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.film.dto.FilmDTO;
import io.renren.modules.film.entity.FilmEntity;
import io.renren.modules.film.excel.FilmExcel;
import io.renren.modules.film.service.FilmService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
@RestController
@RequestMapping("film")
@Api(tags="")
public class FilmController {
    @Autowired
    private FilmService filmService;
    /**
     * <p>电影服务分页查询</p>
     */
    @GetMapping("pageInfo")
    public Result<PageData<FilmDTO>> pageInfo(Integer pageNo,Integer pageSize){
        PageData<FilmDTO> pageInfo = filmService.pageInfo(pageNo,pageSize);
        return new Result<PageData<FilmDTO>>().ok(pageInfo);
    }

    /**
     * <p>插入电影信息</p>
     */

    @PostMapping("/addFilmMessage")
    public int addFilmMessage(@RequestBody FilmEntity filmEntity) {
        return filmService.addFilmMessage(filmEntity);
    }

    /**
     * <p>删除一个电影信息</p>
     */
    @DeleteMapping("/deleteFilmMessage/{filmId}")
    public int deleteFilmMessage(@PathVariable Integer filmId){

        return filmService.deleteFilmMessage(filmId);
    }

//    @PutMapping()
//    public int updateFimeMessage(@PathVariable Integer filmId){
//        return filmService.updateFimeMessage(filmId);
//    }


    /**
     *<p> 查询电影</p>
     */
    @GetMapping("checkFilmMessage")
    public FilmDTO checkFilmMessage(  Integer filmId){
        return filmService.checkFilmMessage(filmId);
    }








    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
        @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
        @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
    })
    @RequiresPermissions("film:film:page")
    public Result<PageData<FilmDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params){
        PageData<FilmDTO> page = filmService.page(params);

        return new Result<PageData<FilmDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("film:film:info")
    public Result<FilmDTO> get(@PathVariable("id") Long id){
        FilmDTO data = filmService.get(id);

        return new Result<FilmDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("film:film:save")
    public Result save(@RequestBody FilmDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        filmService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("film:film:update")
    public Result update(@RequestBody FilmDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        filmService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("film:film:delete")
    public Result delete(@RequestBody Long[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        filmService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("film:film:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<FilmDTO> list = filmService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, FilmExcel.class);
    }

}