package io.renren.modules.film.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 苏双鸽
 * @version v1.0.0
 * @className FilmType.java
 * @description
 * @date 2022-11-17 11:46:47
 */
public class FilmType {
    public final static Map<Integer, String> nameMap = new HashMap<>();

    static {
        nameMap.put(1, "动作");
        nameMap.put(2, "爱情");
        nameMap.put(3, "喜剧");
        nameMap.put(4, "科幻");
        nameMap.put(5, "恐怖");
        nameMap.put(6, "战争");
        nameMap.put(7, "动漫");
    }
}
