package io.renren.modules.film.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.film.entity.FilmEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
@Mapper
public interface FilmDao extends BaseDao<FilmEntity> {
	
}