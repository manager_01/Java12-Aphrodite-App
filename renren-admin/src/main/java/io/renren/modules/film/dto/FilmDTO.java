package io.renren.modules.film.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
@Data
@ApiModel(value = "")
public class FilmDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "电影编号")
	private Integer filmId;

	@ApiModelProperty(value = "电影名")
	private String filmName;

	@ApiModelProperty(value = "电影时长")
	private String filmTime;

	@ApiModelProperty(value = "上映时间")
	private Date playTime;

	@ApiModelProperty(value = "电影图片")
	private String posterUrl;

	@ApiModelProperty(value = "电影种类")
	private String filmCategory;

	@ApiModelProperty(value = "导演")
	private String director;

	@ApiModelProperty(value = "编剧")
	private String protagonist;

	@ApiModelProperty(value = "电影种类")
	private String screenwriters;

	@ApiModelProperty(value = "电影简介")
	private String filmReview;

	@ApiModelProperty(value = "票价")
	private Double money;


}