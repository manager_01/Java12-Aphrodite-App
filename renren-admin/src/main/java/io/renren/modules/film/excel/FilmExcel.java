package io.renren.modules.film.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
@Data
public class FilmExcel {
    @Excel(name = "电影编号")
    private Integer filmId;
    @Excel(name = "电影名")
    private String filmName;
    @Excel(name = "电影时长")
    private String filmTime;
    @Excel(name = "上映时间")
    private Date playTime;
    @Excel(name = "电影图片")
    private String posterUrl;
    @Excel(name = "电影种类")
    private String filmCategory;
    @Excel(name = "导演")
    private String director;
    @Excel(name = "编剧")
    private String protagonist;
    @Excel(name = "电影种类")
    private String screenwriters;
    @Excel(name = "电影简介")
    private String filmReview;
    @Excel(name = "票价")
    private Double money;

}