package io.renren.modules.film.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.common.page.PageData;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.ConvertUtils;
import io.renren.modules.film.dao.FilmDao;
import io.renren.modules.film.dto.FilmDTO;
import io.renren.modules.film.entity.FilmEntity;
import io.renren.modules.film.service.FilmService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
@Service
public class FilmServiceImpl extends CrudServiceImpl<FilmDao, FilmEntity, FilmDTO> implements FilmService {

    @Override
    public QueryWrapper<FilmEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<FilmEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


    /**
     * <p>电影信息分页</p>
     */
    @Override
    public PageData<FilmDTO> pageInfo(Integer pageNo, Integer pageSize) {
        IPage<FilmEntity> page = new Page<>(pageNo, pageSize);
        IPage<FilmEntity> result = baseDao.selectPage(page, null);
        List<FilmEntity> FilmEntityList = result.getRecords();
        List<FilmDTO> filmDTOList = ConvertUtils.sourceToTarget(FilmEntityList, FilmDTO.class);
        PageData<FilmDTO> pageData = new PageData<>(filmDTOList, result.getTotal());
        return pageData;
    }

    /**
     * <p>添加电影信息</p>
     */
    @Override
    public int addFilmMessage(FilmEntity filmEntity) {
        int addFilm = baseDao.insert(filmEntity);
        return addFilm;
    }
    /**
     * <p>删除电影信息</p>
     */
    @Override
    public int deleteFilmMessage(Integer filmId) {
        int deleteFilm = baseDao.deleteById(filmId);
        return deleteFilm;


    }

    /**
     * <p>查询电影信息</p>
     *
     */
    @Override
    public FilmDTO checkFilmMessage(Integer filmId) {
        FilmEntity filmEntity = baseDao.selectById(filmId);
        FilmDTO filmDTO = new FilmDTO();
        BeanUtils.copyProperties(filmEntity,filmDTO);
        return filmDTO;
    }
    /**
     * <p>修改电影信息</p>
     */
//    @Override
//    public int updateFimeMessage(Integer filmId) {
//        int update = baseDao.updateById(filmId);
//        return update;
//    }
}