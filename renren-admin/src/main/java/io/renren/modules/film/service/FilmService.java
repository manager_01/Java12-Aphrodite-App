package io.renren.modules.film.service;

import io.renren.common.page.PageData;
import io.renren.common.service.CrudService;
import io.renren.modules.film.dto.FilmDTO;
import io.renren.modules.film.entity.FilmEntity;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-17
 */
public interface FilmService extends CrudService<FilmEntity, FilmDTO> {


    PageData<FilmDTO> pageInfo(Integer pageNo, Integer pageSize);

    int addFilmMessage(FilmEntity filmEntity);

    int deleteFilmMessage(Integer filmId);

    FilmDTO checkFilmMessage(Integer filmId);

//    int updateFimeMessage(Integer filmId);
}