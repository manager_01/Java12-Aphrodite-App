package io.renren.modules.orders.service;

import io.renren.common.page.PageData;
import io.renren.common.service.CrudService;
import io.renren.modules.orders.dto.OrdersDTO;
import io.renren.modules.orders.entity.OrdersEntity;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-21
 */
public interface OrdersService extends CrudService<OrdersEntity, OrdersDTO> {

    PageData<OrdersDTO> pageInfo(Integer pageNo, Integer pageSize);
}