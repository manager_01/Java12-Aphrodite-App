package io.renren.modules.orders.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.orders.entity.OrdersEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-21
 */
@Mapper
public interface OrdersDao extends BaseDao<OrdersEntity> {
	
}