package io.renren.modules.orders.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-21
 */
@Data
@ApiModel(value = "")
public class OrdersDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "订单编号")
	private Integer aphroditeOrderId;

	@ApiModelProperty(value = "电影名")
	private String filmName;

	@ApiModelProperty(value = "影厅编号")
	private Integer roomId;

	@ApiModelProperty(value = "票价")
	private Double money;

	@ApiModelProperty(value = "创建时间")
	private Date creatTime;

	@ApiModelProperty(value = "电影院名")
	private String filmRoomName;

	@ApiModelProperty(value = "场次编号")
	private Integer sessionId;

	@ApiModelProperty(value = "座位编号")
	private Integer seatId;


}