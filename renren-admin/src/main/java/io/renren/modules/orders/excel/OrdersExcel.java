package io.renren.modules.orders.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-21
 */
@Data
public class OrdersExcel {
    @Excel(name = "订单编号")
    private Integer aphroditeOrderId;
    @Excel(name = "电影名")
    private String filmName;
    @Excel(name = "影厅编号")
    private Integer roomId;
    @Excel(name = "票价")
    private Double money;
    @Excel(name = "创建时间")
    private Date creatTime;
    @Excel(name = "电影院名")
    private String filmRoomName;
    @Excel(name = "场次编号")
    private Integer sessionId;
    @Excel(name = "座位编号")
    private Integer seatId;

}