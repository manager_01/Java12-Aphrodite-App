package io.renren.modules.orders.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-21
 */
@Data
@TableName("orders")
public class OrdersEntity {

    /**
     * 订单编号
     */
	private Integer aphroditeOrderId;
    /**
     * 电影名
     */
	private String filmName;
    /**
     * 影厅编号
     */
	private Integer roomId;
    /**
     * 票价
     */
	private Double money;
    /**
     * 创建时间
     */
	private Date creatTime;
    /**
     * 电影院名
     */
	private String filmRoomName;
    /**
     * 场次编号
     */
	private Integer sessionId;
    /**
     * 座位编号
     */
	private Integer seatId;
}