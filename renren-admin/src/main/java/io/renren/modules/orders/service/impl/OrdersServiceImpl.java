package io.renren.modules.orders.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.common.page.PageData;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.ConvertUtils;
import io.renren.modules.orders.dao.OrdersDao;
import io.renren.modules.orders.dto.OrdersDTO;
import io.renren.modules.orders.entity.OrdersEntity;
import io.renren.modules.orders.service.OrdersService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2022-11-21
 */
@Service
public class OrdersServiceImpl extends CrudServiceImpl<OrdersDao, OrdersEntity, OrdersDTO> implements OrdersService {

    @Override
    public QueryWrapper<OrdersEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<OrdersEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


    @Override
    public PageData<OrdersDTO> pageInfo(Integer pageNo, Integer pageSize) {
        Page<OrdersEntity> page = new Page<>(pageNo,pageSize);

//        LambdaQueryWrapper<OrdersEntity> objectLambdaQueryWrapper = new LambdaQueryWrapper<>();

//        objectLambdaQueryWrapper.eq(OrdersEntity::getFilmName,"1");

        Page<OrdersEntity> ordersEntityPage = baseDao.selectPage(page, null);

        List<OrdersEntity> records = ordersEntityPage.getRecords();

        List<OrdersDTO> ordersDTOS = ConvertUtils.sourceToTarget(records, OrdersDTO.class);

        PageData<OrdersDTO> ordersDTOPageData = new PageData<>(ordersDTOS, ordersEntityPage.getTotal());

        return ordersDTOPageData;


    }
}